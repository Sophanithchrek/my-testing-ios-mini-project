//
//  ImageViewController.swift
//  TestBrowseAndUploadImage
//
//  Created by SOPHANITH CHREK on 9/12/20.
//

import UIKit
import Kingfisher

class ImageViewController: UIViewController {

    var avatarData = [Avatar]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        fetchImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.dataSource = self
        tableView.delegate = self
    }

    func fetchImage() {
        let url = URL(string: "http://110.74.194.124:3000/api/images")!
        let request = URLRequest(url: url)
        let session = URLSession.shared
        let task = session.dataTask(with: request) {
            (data, response, error) in
            if let err = error { print("It is error: \(err)") }
            guard let data = data else { return }
            do {
                let response = try JSONDecoder().decode([Avatar].self, from: data)
                for rec in response {
                    self.avatarData.append(contentsOf: [rec])
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            } catch let error {
                print(error)
            }
        }
        task.resume()
    }
    
    @IBAction func btnAddPressed(_ sender: Any) {
        let secondVC = storyboard?.instantiateViewController(withIdentifier: "uploadForm") as! ViewController
        secondVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(secondVC, animated: true)
    }
    
}

extension ImageViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.avatarData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ImageTableViewCell
        cell.imageViewArticle.kf.setImage(with: URL(string: avatarData[indexPath.row].url), placeholder: UIImage(named: "image-not-available"), options: nil, progressBlock: nil) { result in
            switch result {
            case .success( _):
                print("It success")
                break
            case .failure(let error):
                if error.isInvalidResponseStatusCode {
                    cell.imageViewArticle.image = UIImage(named: "image-not-available")
                }
            }
        }
        return cell
    }

}

extension ImageViewController: UITableViewDelegate {
    
}
