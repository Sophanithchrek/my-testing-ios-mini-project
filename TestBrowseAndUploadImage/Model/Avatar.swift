//
//  Avatar.swift
//  TestBrowseAndUploadImage
//
//  Created by SOPHANITH CHREK on 9/12/20.
//

import Foundation

// MARK: - Avartar API
struct Avatar: Codable {
    let id: String
    let url: String
    let createdAt, updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case url, createdAt, updatedAt
        case v = "__v"
    }
}

// get all image
// http://110.74.194.124:3000/api/images

// upload image
// need to have image first
// curl -X POST "http://110.74.194.124:3000/api/images" -H "accept: application/json" -H "Content-Type: multipart/form-data" -F "image=@image-not-available.png;type=image/png"
// http://110.74.194.124:3000/api/images
